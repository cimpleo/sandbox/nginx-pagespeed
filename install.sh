# Init Script folder
MAIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Moving nginx.service
echo "Install nginx.service"
cp ${MAIN_DIR}/conf/nginx.service /lib/systemd/system/nginx.service

# Create sites folder
echo "Create sites folder"
mkdir -p /etc/nginx/snippets
mkdir -p /etc/nginx/conf.d
mkdir -p /etc/nginx/sites-available
mkdir -p /etc/nginx/sites-enabled
mkdir -p /var/www/html

# Copy nginx.conf
echo "Copy nginx.conf"
cp ${MAIN_DIR}/conf/nginx.conf /etc/nginx/

# Copy conf.d
echo "Copy conf.d"
cp -R ${MAIN_DIR}/conf.d/*.conf /etc/nginx/conf.d/

# Copy snippets
echo "Copy snippets"
cp -R ${MAIN_DIR}/snippets/*.conf /etc/nginx/snippets/

# Copy default-sites
echo "Copy default-sites"
cp ${MAIN_DIR}/sites-available/default /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default


# Reload systemctl
echo "Reload systemctl"
systemctl daemon-reload

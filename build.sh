#!/bin/bash

# Installing dependencies
sudo apt update
sudo apt install -q -y build-essential zlib1g-dev libpcre3 libpcre3-dev unzip uuid-dev libssl-dev libgd-dev libgeoip-dev libxml2-dev libxslt1-dev

# Making directory for build
MAIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BUILD_DIR=${MAIN_DIR}/temp
mkdir -p $BUILD_DIR
cd $BUILD_DIR

echo $BUILD_DIR

# Fetching ngx_pagespeed module source code
NPS_VERSION=1.13.35.2-stable
cd $BUILD_DIR
wget https://github.com/apache/incubator-pagespeed-ngx/archive/v${NPS_VERSION}.zip \
    && unzip -qq v${NPS_VERSION}.zip \
    && rm v${NPS_VERSION}.zip

nps_dir=$(find . -name "*pagespeed-ngx-${NPS_VERSION}" -type d)
cd "$nps_dir"
NPS_RELEASE_NUMBER=${NPS_VERSION/stable/}
psol_url=https://dl.google.com/dl/page-speed/psol/${NPS_RELEASE_NUMBER}.tar.gz
[ -e scripts/format_binary_url.sh ] && psol_url=$(scripts/format_binary_url.sh PSOL_BINARY_URL)

wget ${psol_url} \
    && tar -xzf $(basename ${psol_url}) \
    && rm $(basename ${psol_url})

# Fetching pagespeed_cache_purge module
cd $BUILD_DIR
wget http://labs.frickle.com/files/ngx_cache_purge-2.3.tar.gz \
    && tar -zxf ngx_cache_purge-2.3.tar.gz \
    && mv ngx_cache_purge-2.3 ngx_cache_purge \
    && rm ngx_cache_purge-2.3.tar.gz

# Fetching nginx source code
NGINX_VERSION=1.14.2
mkdir -p /var/cache/nginx/
mkdir -p /var/log/nginx/


cd $BUILD_DIR
wget http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz \
    && tar -xzf nginx-${NGINX_VERSION}.tar.gz \
    && rm nginx-${NGINX_VERSION}.tar.gz

cd nginx-${NGINX_VERSION}/

./configure \
    --prefix=/etc/nginx \
    --sbin-path=/usr/sbin/nginx \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/run/nginx.lock \
    --http-client-body-temp-path=/var/cache/nginx/client_temp \
    --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
    --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
    --user=nginx \
    --group=nginx \
    --with-pcre-jit \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_auth_request_module \
    --with-http_realip_module \
    --with-http_dav_module \
    --with-http_slice_module \
    --with-http_addition_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-threads \
    --with-http_geoip_module=dynamic \
    --with-file-aio \
    --with-http_sub_module \
    --with-http_xslt_module=dynamic \
    --with-http_image_filter_module=dynamic \
    --with-http_v2_module \
    --with-stream=dynamic \
    --with-stream_ssl_module \
    --with-mail=dynamic \
    --with-mail_ssl_module \
    --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -m64 -mtune=native' --add-module=$BUILD_DIR/ngx_cache_purge --add-module=$BUILD_DIR/$nps_dir ${PS_NGX_EXTRA_FLAGS}

# Make NGINX with module
make

# Install NGINX with PS module
sudo make install

# Remove build dir
rm -rf $BUILD_DIR

# Run installer
bash ${MAIN_DIR}/install.sh
